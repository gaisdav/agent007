const {defaults} = require('jest-config');

module.exports = {
    verbose: true,
    setupFilesAfterEnv: ['<rootDir>setupEnzymeTests.js'],
    moduleFileExtensions: [...defaults.moduleFileExtensions, 'tsx', 'jsx'],
    coverageDirectory: '<rootDir>/coverage/',
    collectCoverageFrom: ['src/**/*.{jsx,tsx}', '!**/node_modules/**', '!src/**/*.d.ts'],
    moduleNameMapper: {
        '^.+\\.(css|less|scss|sass)$': 'babel-jest',
        '^Common(.*)$': '<rootDir>/src/Common$1',
        '^Components(.*)$': '<rootDir>/src/Components$1',
        '^Routes(.*)$': '<rootDir>/src/Routes$1',
        '^Store(.*)$': '<rootDir>/src/Store$1',
    },
    notify: true,
};
