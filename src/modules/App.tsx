import Main from './main/Main';
import React from 'react';
import './App.scss';

const App = () => <Main />;

export default App;
