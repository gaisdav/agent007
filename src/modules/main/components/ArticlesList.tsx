import {EStoreStatus, IStore} from 'store/interfaces';
import React, {FC, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import Article from './Article';
import ArticlesListActions from 'store/actions/ArticlesListActions';
import '../styles/ArticlesList.scss';

const ArticlesList: FC = () => {
    const {articlesList} = useSelector((state: IStore) => state);
    const actions = new ArticlesListActions(useDispatch());

    const {status, data: articles} = articlesList;

    useEffect(() => {
        actions.getArticles();
    }, []);

    switch (status) {
        case EStoreStatus.LOADING:
        case EStoreStatus.INIT:
            return <div>Loading...</div>;
        case EStoreStatus.SUCCESS:
            return (
                <div>
                    {articles.map((article) => (
                        <Article key={article.id} article={article} />
                    ))}
                </div>
            );
        case EStoreStatus.ERROR:
            return <div>Error</div>;
        default:
            return null;
    }
};

export default ArticlesList;
