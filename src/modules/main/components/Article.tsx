import React, {FC} from 'react';
import {IArticle} from '../interfaces';

interface IProps {
    article: IArticle;
}

const Article: FC<IProps> = (props: IProps) => {
    const {article} = props;

    return (
        <article>
            <a className="title" href={article.link} target="_blank" rel="noreferrer noopener">
                {article.title}
            </a>
            <div className="hubs">{article.hubs.join(', ')}</div>
            <div className="date">{article.date}</div>
            <button className="favorites">{article.favorites ? 'удалить из избранных' : 'добавить в избранные'}</button>
        </article>
    );
};

export default Article;
