/**
 * Интерфейс статьи.
 */
export interface IArticle {
    _id: string;
    id: string;
    title: string;
    link: string;
    hubs: string[];
    favorites: boolean;
    date: string;
}
