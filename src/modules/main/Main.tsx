import ArticlesList from './components/ArticlesList';
import React from 'react';
import './styles/Main.scss';

const Main = () => <ArticlesList />;
export default Main;
