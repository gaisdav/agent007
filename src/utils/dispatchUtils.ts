import {Dispatch} from 'redux';
import {EStoreStatus} from 'store/interfaces';

export interface IAction<T> {
    type: string;
    payload: Promise<T>;
}

export class DispatchUtils<T> {
    dispatch: Dispatch;

    constructor(dispatch: Dispatch) {
        this.dispatch = dispatch;
    }

    async(action: IAction<T>): void {
        const {type, payload} = action;

        this.dispatch({
            type: `${type}_${EStoreStatus.LOADING}`,
        });

        payload
            .then((data: T) => {
                this.dispatch({
                    type: `${type}_${EStoreStatus.SUCCESS}`,
                    payload: data,
                });
            })
            .catch((err) => {
                this.dispatch({
                    type: `${type}_${EStoreStatus.ERROR}`,
                });
                throw Error(err);
            });
    }

    // simple() {}
}
