import {EServiceEndPoint} from './EServiceEndPoint';

class FetchUtils<T> {
    /**
     * get
     */
    public async get(endPoint: EServiceEndPoint): Promise<T> {
        const data = await fetch(endPoint);
        return await data.json();
    }

    /**
     * post
     */
    // public async post(endPoint: string): Promise<T> {
    //     const data = await fetch(endPoint);
    //     return await data.json();
    // }
}

export default FetchUtils;
