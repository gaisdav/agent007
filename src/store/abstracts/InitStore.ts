import {EStoreStatus, IStoreBranch} from '../interfaces';

class InitStore<S> implements IStoreBranch<S> {
    data: S;
    status: EStoreStatus;

    constructor(initState: S) {
        this.status = EStoreStatus.INIT;
        this.data = initState;
    }
}

export default InitStore;
