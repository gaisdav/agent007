import {ArticlesListReducer} from './reducers/ArticlesListReducer';
import {IStore} from './interfaces';
import {combineReducers} from 'redux';

export default combineReducers<IStore>({
    articlesList: ArticlesListReducer,
});
