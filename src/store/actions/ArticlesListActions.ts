import {Dispatch} from 'redux';
import {DispatchUtils} from 'utils/dispatchUtils';
import {EServiceEndPoint} from 'utils/EServiceEndPoint';
import FetchUtils from 'utils/fetchUtils';
import {IArticle} from 'modules/main/interfaces';

class ArticlesListActions {
    dispatchUtils: DispatchUtils<IArticle[]>;
    fetchUtils: FetchUtils<IArticle[]> = new FetchUtils();

    constructor(dispatch: Dispatch) {
        this.dispatchUtils = new DispatchUtils<IArticle[]>(dispatch);
    }

    getArticles() {
        this.dispatchUtils.async({
            type: 'GET_ARTICLES',
            payload: this.fetchUtils.get(EServiceEndPoint.ARTICLES),
        });
    }
}

export default ArticlesListActions;
