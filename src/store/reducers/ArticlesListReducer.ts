import {IStoreBranch, EStoreStatus, IReducersAction} from 'store/interfaces';
import {IArticle} from 'modules/main/interfaces';
import InitStore from 'store/abstracts/InitStore';

const initialState: any = new InitStore<IArticle[]>([]);

/**
 * Сохраняет список статей.
 *
 * @param state
 * @param action
 */
export function ArticlesListReducer(
    state: IStoreBranch<IArticle[]> = initialState,
    action: IReducersAction<IArticle[]>
): IStoreBranch<IArticle[]> {
    switch (action.type) {
        case 'GET_ARTICLES_LOADING':
            return {...state, status: EStoreStatus.LOADING};
        case 'GET_ARTICLES_SUCCESS':
            return {...state, status: EStoreStatus.SUCCESS, data: action.payload};
        case 'GET_ARTICLES_ERROR':
            return {...state, status: EStoreStatus.ERROR};
        case 'RESET_ARTICLES_LIST':
            return {...state, data: []};
        default:
            return state;
    }
}
