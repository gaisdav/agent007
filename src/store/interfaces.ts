import {IArticle} from 'modules/main/interfaces';

/**
 * Статусы состояния хранилища.
 */
export enum EStoreStatus {
    INIT = 'INIT',
    LOADING = 'LOADING',
    SUCCESS = 'SUCCESS',
    ERROR = 'ERROR',
}

/**
 * Интерфейс ветки хранилища.
 */
export interface IStoreBranch<S> {
    data: S;
    status: EStoreStatus;
}

/**
 * Интерфейс редюсера
 */
export interface IReducersAction<P> {
    type: string;
    payload: P;
}

/**
 * Интерфейс хранилища.
 */
export interface IStore {
    articlesList: IStoreBranch<IArticle[]>;
}
