const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const pkg = require('../package.json');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');

module.exports = {
    resolve: {
        extensions: ['.ts', '.tsx', '.js', 'jsx'],
        alias: {
            utils: path.join(__dirname, '../src/utils'),
            store: path.join(__dirname, '../src/store'),
        },
    },
    context: path.resolve(__dirname, '../'),
    entry: './src/index.tsx',
    output: {
        path: path.join(__dirname, '../dist'),
        filename: pkg.name + '.bandle.js',
    },
    module: {
        rules: [
            {
                test: /\.(scss|css)$/,
                use: ['style-loader', 'css-loader', 'sass-loader', 'cache-loader'],
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: ['file-loader'],
            },
            {
                test: /\.eot|ttf|woff|woff2($|\?)/,
                use: {
                    loader: 'file-loader',
                    options: {
                        outputPath: 'fonts/',
                    },
                },
            },
            {
                test: /\.(ts|tsx)$/,
                use: ['cache-loader', 'ts-loader'],
                exclude: /node_modules/,
            },
        ],
    },
    plugins: [
        new ProgressBarPlugin(),
        new HtmlWebpackPlugin({
            template: './src/index.html',
        }),
    ],
};
