const TerserPlugin = require('terser-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const commonConfig = require('./webpack.common.config');
const merge = require('webpack-merge');

module.exports = merge(commonConfig, {
    mode: 'production',
    optimization: {
        minimize: true,
        minimizer: [
            new TerserPlugin({
                terserOptions: {
                    extractComments: 'all',
                    compress: {
                        unsafe: true,
                        sequences: true,
                        booleans: true,
                        inline: true,
                        keep_fargs: false,
                        drop_console: true,
                    },
                },
                parallel: true,
                extractComments: true,
                cache: true,
            }),
        ],
    },
    plugins: [...commonConfig.plugins, new CleanWebpackPlugin()],
});
