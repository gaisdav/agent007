const commonConfig = require('./webpack.common.config');
const merge = require('webpack-merge');

module.exports = merge(commonConfig, {
    mode: 'development',
    devtool: 'source-map', // проверить, возможно лучше будет source-map хот я и медленее
    devServer: {
        open: true,
        hot: true,
        compress: true,
        overlay: true,
        stats: {
            colors: true,
        },
    },
});
