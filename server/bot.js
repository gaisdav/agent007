/* eslint-disable max-lines */
// const Telegraf = require('telegraf');
const cheerio = require('cheerio');
const fetch = require('node-fetch');
const MongoClient = require('mongodb').MongoClient;
const moment = require('moment');
// const SocksProxyAgent = require('socks-proxy-agent');

// const agent = new SocksProxyAgent({
//   host: '185.153.198.177',
//   port: '46990',
// });

moment.locale('ru');

const HUBS = [
    'javascript',
    'react',
    'css',
    'vue',
    'фронтенд',
    'html5',
    'html',
    'node.js',
    'es6',
    'микросервисы',
    'совершенный код',
    'typescript',
    'express.js',
    'web-разработка',
    'nodejs',
    'разработка',
    'npm',
    'webpack',
    'react.js',
    'redux',
    'reactjs',
    'frontend',
    'react native',
    'svelte',
    'sveltejs',
    'three.js',
    'canvas',
    'svg',
    'mongodb',
    'react-native',
    'reactnative',
    'разработка веб-сайтов',
    'карьера в it-индустрии',
];

const URL = 'mongodb://localhost:27017/';
const mongoClient = new MongoClient(URL, {useUnifiedTopology: true});
// const CHAT_ID = '368899378';

// let clientDb;
let collection;

mongoClient.connect(function(err, client) {
    // clientDb = client;
    const db = client.db('postsdb');
    collection = db.collection('posts');
    // collection.find().toArray(function(err, results){
    //   console.log(results);
    //   client.close();
    // });
});

// const bot = new Telegraf('990107752:AAGlnp-HH2Rx9975vvs_yFRO8Q6KEK_KLFo', {
//   webhookReply: true,
//   // telegram: {
//   //   agent: agent
//   // }
// })
// bot.on('text', (ctx) => { ctx.reply('Не войлнуйся, живой еще)') })
// bot.launch().then(() => console.log('Работаем!'))

const getPosts = () => {
    console.log('вызов getPosts()');
    fetch('https://habr.com/ru/all/')
        .then((res) => res.text())
        .then((body) => {
            const $ = cheerio.load(body, {
                normalizeWhitespace: true,
                xmlMode: true,
            });

            let postsList = $('div.posts_list > ul.content-list.content-list_posts.shortcuts_items')
                .find('li.content-list__item.content-list__item_post.shortcuts_item')
                .toArray();

            postsList.forEach(async (element) => {
                const postId = element.attribs.id;
                let isNewPost = true;
                let postHubs = $(`#${postId} > .post_preview > .post__hubs`)
                    .text()
                    .split(',')
                    .map((item) => item.toLocaleLowerCase().trim());

                const hasMyHub = postHubs.some((item) => HUBS.includes(item));

                if (!!postId && hasMyHub) {
                    isNewPost = !(await collection.findOne({id: postId}));

                    if (isNewPost) {
                        let postTitle = $(`#${postId} > .post_preview > .post__title`)
                            .text()
                            .trim();
                        let postLink = $(`#${postId} > .post_preview > .post__title > .post__title_link`).attr('href');
                        let currPostObj = {
                            id: postId,
                            title: postTitle,
                            link: postLink,
                            hubs: postHubs,
                            favorites: false,
                            date: moment().format('DD MMMM YYYY'),
                        };

                        collection.insertOne(currPostObj, function(err, result) {
                            if (err) {
                                return console.log('ERROR', err);
                            }
                            console.log(result);
                            // bot.telegram.sendMessage(CHAT_ID, `${postLink}\n\n${postHubs.join(', ')}`);
                            // clientDb.close();
                        });
                    }
                }
            });
        })
        .catch((e) => {
            console.log('error', e);
        });
};

setInterval(getPosts, 50000);

// TODO добавить поиск в базе данных по хештегу
// TODO написать парсер по поиску работы

// удаляет всю коллекцию
// collection.drop(function (err, result) {
//   console.log('drop', result);
//   // client.close();
// });
