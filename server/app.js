const express = require('express');
const localJSON = require('../src/constants.json');

const app = express();

app.all('*', function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With');
    next();
});

app.get('/articles', function(req, response) {
    response.send(localJSON);
});

app.listen(3000, () => {
    console.log('Сервер запущен по адресу http://localhost:3000/.');
});

// // import express from 'express';
// const express = require('express');
// // TODO подключить мангуста
// const MongoClient = require('mongodb').MongoClient;

// const app = express();

// const URL = 'mongodb://localhost:27017/';
// const mongoClient = new MongoClient(URL, {useUnifiedTopology: true});

// let collection;

// app.all('*', function(req, res, next) {
//     res.header('Access-Control-Allow-Origin', '*');
//     res.header('Access-Control-Allow-Headers', 'X-Requested-With');
//     next();
// });

// app.get('/articles', function(req, response) {
//     collection.find().toArray(function(err, results) {
//         response.send(results);
//     });
// });

// mongoClient.connect(function(err, client) {
//     if (err) {
//         console.log(err);
//     }
//     const db = client.db('postsdb');
//     collection = db.collection('posts');

//     app.listen(3000, () => {
//         console.log('Сервер запущен.');
//     });

//     console.log('Подключение к базе данных произведено');
// });
